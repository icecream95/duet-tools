# duet-tools

A collection of tools designed to make working with a bare-metal Linux
installation easier on a Lenovo Chromebook Duet. Some of the tools are
designed to work with sway, but others are compositor-independent.

Some of these tools will also work on other Chromeblets or tablets,
but device paths might need adjusting.

## autorotate.sh

This simple script mimics the autorotate behaviour on Chrome OS. By
default, the orientation is reset when the keyboard is attached.

For it to work, `IIO_CROS_EC_SENSORS` must be enabled in the kernel.

Create the file `/tmp/autorotate.lock` to pause autorotation, and remove
it to unlock the orientation.

## bright.py

This is a simple brightness modification script. Just pass it an
argument such as `1` or `-0.5` for a relative change in brightness.

The backlight file (defaults to
`/sys/class/backlight/backlight_lcd0/brightness`) needs to be writable
by the user executing this script.

Automatic adjustment via the light sensor coming soon...

## ncurses_select.py

This is a mouse (and finger) enabled ncurses script which makes it
easy to implement simple touchscreen-based menus.

Use it like this:

```
$ TITLE="My menu" ./ncurses_select.py Minesweeper Freecell Tetris; echo $?
```

The selected item is returned as the return status. The first menu
item passed in is given return status 2.

## menu.sh

This is a simple menu using ncurses_select.py, with a few useful menu
items.

Rotation settings requires the autorotate script to be running, and
the keyboard toggle is for Squeekboard.
