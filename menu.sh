#!/bin/bash

TERMINAL=konsole

# File for disabling autorotation
LOCK_FILE=/tmp/autorotate.lock

BRIGHTNESS_CHANGE=1

###

while :
do
    if [ -e $LOCK_FILE ]; then
        ROT="Unlock rotation"
    else
        ROT="Lock rotation"
    fi

    nc_sel "Brightness up" "Brightness down" "$ROT" "Toggle keyboard" "htop"
    opt=$?
    echo $opt
    case $opt in
        2)
            ./bright.py $BRIGHTNESS_CHANGE
            ;;
        3)
            ./bright.py -$BRIGHTNESS_CHANGE
            ;;
        4)
            if [ -e $LOCK_FILE ]; then
                rm $LOCK_FILE
            else
                touch $LOCK_FILE
            fi
            ;;
        5)
            VISIBLE="`busctl get-property --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 Visible`"
            if printf "%s" "$VISIBLE" | grep -q true; then
                busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b false
            elif printf "%s" "$VISIBLE" | grep -q false; then
                busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b true
            else
                squeekboard &
                sleep 1
                busctl call --user sm.puri.OSK0 /sm/puri/OSK0 sm.puri.OSK0 SetVisible b true
            fi
            ;;
        6)
            "$TERMINAL" -e htop &
            ;;
    esac
done
