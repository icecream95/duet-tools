#!/bin/sh

# Don't rotate when the keyboard is attached
SKIP_IF_KBD=1

# While this file exists, autorotation will be disabled
LOCK_FILE=/tmp/autorotate.lock

# Threshold value for the accelerometer reading
ACCEL_THRESHOLD=5000

ACCEL=`realpath /sys/bus/platform/devices/cros-ec-accel*/iio:device*`

HAMMER_USB=/sys/bus/usb/devices/usb1/1-1/1-1.1/

###

ORIENT=-1

orient() {
    if [ $ORIENT != $1 ]; then
        ORIENT=$1
        swaymsg -t command output '*' transform $1
    fi
}

check_kbd() {
    if [ $SKIP_IF_KBD != 1 ]; then
        return 1
    fi

    if [ -e $HAMMER_USB ]; then
        orient 270
        return 0
    fi

    return 1
}

while sleep 1; do
    if [ -e $LOCK_FILE ]; then
        continue
    fi

    if check_kbd; then
        continue
    fi

    read ax <$ACCEL/in_accel_x_raw
    read ay <$ACCEL/in_accel_y_raw

#    echo $ax
#    echo $ay

    if [ $ay -gt $ACCEL_THRESHOLD ]; then
        # Normal orientation
        orient 270
    elif [ $ax -gt $ACCEL_THRESHOLD ]; then
        orient 0
    elif [ $ax -lt -$ACCEL_THRESHOLD ]; then
        orient 180
    elif [ $ay -lt -$ACCEL_THRESHOLD ]; then
        orient 90
    fi
done
