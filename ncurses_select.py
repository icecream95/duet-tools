#!/usr/bin/env python3

import curses
import math
import os
import sys

gminl = os.getenv("MINSZ")
if gminl == None:
    gminl = 12
else:
    gminl = int(gminl)

def myhash(mod,add,old,string,ch,const):
    # Not a very good hash function perhaps, but it seems to work
    hash = old*len(string) - 28 + int(ch*17.46*old) + ch*const + int(33*ch/const+old/ch)
    if hash%mod+add == old:
        return (hash+1)%mod+add
    else:
        return hash%mod+add

def dostuff(stdscr, title):
    oy,x = stdscr.getmaxyx()

    ty = min(math.ceil(len(title)/x),oy)

    y = oy - ty

    args = len(sys.argv)

    minl = gminl

    for i in range(args-1):
        if len(sys.argv[i+1])+4 > minl:
            minl = len(sys.argv[i+1])+4

    px = max(x//minl,1)          # Buttons per line
    fpx = (args-2)%px+1          # Buttons on the first line
    py = math.ceil((args-1)/px)  # Number of lines

    rx = x/px                    # Button width

    ry = y/py                    # Button height
    if ry < 1:
        minl = 1 # Change this for tighter or wider spacing

        for i in range(args-1):
            if len(sys.argv[i+1])+1 > minl:
                minl = len(sys.argv[i+1])+1

        px = max(x//minl,1) # Buttons per line
        fpx = (args-2)%px+1 # Buttons on the first line
        py = math.ceil((args-1)/px) # Number of lines

        rx = x/px # Button width
        ry = y/py                    # Button height
        if ry < 1:
            ry = 1
            for i in range(7):
                curses.init_pair(i+1,7,1)
        else:
            for i in range(7):
                curses.init_pair(i+1,0,i+1)
    else:
        for i in range(7):
                curses.init_pair(i+1,0,i+1)

    argcount = 0
    pair = 0
    for i in range(args-1+px-fpx):
        if i >= fpx and i < px:
            continue
        argcount += 1
        cx = i%px                # Column number of button
        cy = i//px               # Row number of button
        if i >= px:
            lx = int(rx*cx)      # X position of button
            lx2 = int(rx*(cx+1)) # X position of end of button
        else:
            lx = int(x/fpx*cx)
            lx2 = int(x/fpx*(cx+1))
        dx = lx2-lx              # Width of button
        pair = myhash(7,1,pair,sys.argv[argcount],argcount,args)
        try:
            stdscr.addstr(ty+int((int(ry*(cy+1))-int(ry*cy))/2)+int(cy*ry),lx+int((dx-len(sys.argv[argcount]))/2),sys.argv[argcount])
            for j in range(int(ry*(cy+1))-int(ry*cy)):
                stdscr.chgat(ty+j+int(cy*ry),lx,dx,curses.color_pair(pair))
        except Exception:
            pass

    stdscr.move(0,0)
    try:
        stdscr.addstr(title,curses.color_pair(7))
    except Exception:
        pass

    stdscr.refresh()
    while True:
        event = stdscr.getch()
        if event == -1:
            y2,x2 = stdscr.getmaxyx()
            if oy != y2 or x != x2:
                raise InterruptedError('Restart display')
        elif event == curses.KEY_MOUSE:
            try:
                _, mx, my, _, _ = curses.getmouse()
            except Exception:
                continue
            my = int(my/ry)
            if my == 0:
                mx = int(mx*fpx/x)
            else:
                mx = int(mx*px/x)
            i = my*px+mx
            if my > 0:
                i += fpx-px

            return i+1
            break

def main(stdscr):
    curses.mousemask(1)
    curses.halfdelay(1)

    title = os.getenv("TITLE")
    if title == None:
        title = ""

    while True:
        stdscr.clear()
        try:
            a = dostuff(stdscr, title)
            if a and a < len(sys.argv):
                sys.exit(a+1)
        except InterruptedError:
            pass

    stdscr.getkey()

if len(sys.argv) < 2:
    print("Usage: [TITLE='Title'] " + sys.argv[0] + " 'OPTION'...")
else:
    curses.wrapper(main)
