#!/usr/bin/env python

import sys

BRIGHTNESS_FILE = "/sys/class/backlight/backlight_lcd0/brightness"

MAX_BRIGHTNESS = 1023

if len(sys.argv) != 2:
    print("Usage: bright.py BRIGHTNESS_CHANGE")
else:
    with open(BRIGHTNESS_FILE, "r+") as f:

        bright = int(f.readline().strip())

        change = float(sys.argv[1])

        new = min(MAX_BRIGHTNESS,
                  int(max(bright, 1) * pow(2, change)))

        f.seek(0)
        f.write(str(new))
